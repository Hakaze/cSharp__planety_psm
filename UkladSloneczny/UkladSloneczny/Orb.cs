﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace UkladSloneczny
{
    public class Orb<T>
    {
        public string name { get; set; }
        public static int ID = 0;
        public int planetID { get; set; }
        public T mass { get; set; }
        public T radius { get; set; }
        public T radiusForDrawing { get; set; }
        public T[] currentV { get; set; }
        public T[] position { get; set; }
        public T[] force { get; set; }
        public IList<T[]> trajectory { get; set; }
        public bool isMoon { get; set; }
        public Orb<T> homePlanetForMoon { get; set; }

        public Orb(string name, T mass, T radius, T radiusForDrawing, T vX, T vY, T posX, T posY)
        {
            this.name = name;
            this.mass = mass;
            this.radius = radius;
            this.radiusForDrawing = radiusForDrawing;
            T[] speed = { vX, vY };
            T[] position = { posX, posY };
            this.currentV = speed;
            this.position = position;
            planetID = ID;
            ID++;

            force = new T[2];
            trajectory = new List<T[]>();
            isMoon = false;
        }
        public Orb(string name, T mass, T radius, T radiusForDrawing, T vX, T vY, T posX, T posY, Orb<T> homePlanetForMoon)
        {
            this.name = name;
            this.mass = mass;
            this.radius = radius;
            this.radiusForDrawing = radiusForDrawing;
            T[] speed = { vX, vY };
            T[] position = { posX, posY };
            this.currentV = speed;
            this.position = position;
            planetID = ID;
            ID++;

            force = new T[2];
            trajectory = new List<T[]>();
            this.homePlanetForMoon = homePlanetForMoon;
            isMoon = true;
        }
    }
}
