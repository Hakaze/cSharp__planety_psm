﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace UkladSloneczny
{
    public partial class mainWindow : Form
    {
        private BackgroundWorker bw;
        public int limit { get; set; }
        public double deltaT { get; set; }
        OrbSystem orbSystem;
        public Graphics eulerGr { get; set; }
        public Pen blackPen { get; set; }
        bool beforeStart = true;
        Double max;
        Double minPanelDim;
        Double scale;

        int offsetX;
        int offsetY;
        int currentIndex;

        public mainWindow()
        {
            InitializeComponent();
            InitializeOrbSystem();
        }
        public void InitializeOrbSystem()
        {

            limit = 1000000;
            limitTextBox.Text = limit + "";
            deltaT = 10000;
            deltaTTextBox.Text = deltaT + "";

            
            
            eulerGr = animationPictureBox.CreateGraphics();
            blackPen = new Pen(Color.Black);

        }
        private void prepareOrbSystem()
        {
            orbSystem = new OrbSystem(deltaT);
            Orb<Double> sun = new Orb<Double>("Słońce", 1.98855 * Math.Pow(10, 30), 696342000, 50, 0, 0, 0, 0);
            Orb<Double> mercury = new Orb<Double>("Merkury", 330 * Math.Pow(10, 21), 2439500, 5, orbSystem.calcVCosm(57909170000, sun.mass), 0, 0, 57909170000);
            Orb<Double> wenus = new Orb<Double>("Wenus", 4.8685 * Math.Pow(10, 24), 6050000, 8, orbSystem.calcVCosm(108208926000, sun.mass), 0, 0, 108208926000);
            Orb<Double> earth = new Orb<Double>("Ziemia", 5974 * Math.Pow(10, 21), 6378000, 10, orbSystem.calcVCosm(147098291000, sun.mass), 0, 0, 147098291000);
            Orb<Double> moon = new Orb<Double>("Ksiezyc", 7.347673 * Math.Pow(10, 22), 1737064, 2, orbSystem.calcVCosm(384403000, earth.mass) + earth.currentV[0], 0, 0, 147098291000 + 384403000, earth);
            Orb<Double> mars = new Orb<Double>("Mars", 641 * Math.Pow(10, 21), 3402500, 6, orbSystem.calcVCosm(227936637000, sun.mass), 0, 0, 227936637000);
            Orb<Double> jupiter = new Orb<Double>("Jowisz", 1.8986 * Math.Pow(10, 27), 71492000, 20, orbSystem.calcVCosm(778412020000, sun.mass), 0, 0, 778412020000);
            Orb<Double> ganimedes = new Orb<Double>("Ganimedes", 1.4819 * Math.Pow(10, 23), 2634000, 2, orbSystem.calcVCosm(1070400000, jupiter.mass) + jupiter.currentV[0], 0, 0, 778412020000 + 1070400000, jupiter);
            Orb<Double> saturn = new Orb<Double>("Saturn", 568516 * Math.Pow(10, 21), 60268000, 12, orbSystem.calcVCosm(1426725413000, sun.mass), 0, 0, 1426725413000);

            orbSystem.addPlanet(sun);
            orbSystem.addPlanet(mercury);
            orbSystem.addPlanet(wenus);
            orbSystem.addPlanet(earth);
            orbSystem.addPlanet(moon);
            orbSystem.addPlanet(mars);
            //orbSystem.addPlanet(jupiter);
            //orbSystem.addPlanet(ganimedes);
            //orbSystem.addPlanet(saturn);
        }
        private void show()
        {

            bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;
            bw.DoWork += new DoWorkEventHandler(
            delegate (object o, DoWorkEventArgs args)
            {
                BackgroundWorker b = o as BackgroundWorker;
                int i = 0;
                //for (int i = 0; i < limit; i++)
                while(i<limit && !bw.CancellationPending)
                {
                    this.Invoke(new MethodInvoker(() =>
                    {
                        if (i % 100 == 0)
                        {
                            speedXTextBox.Text = orbSystem.orbList[currentIndex].currentV[0] / 1000 + " km/s";
                            speedYTextBox.Text = orbSystem.orbList[currentIndex].currentV[1] / 1000 + " km/s";
                            distanceTextBox.Text = Math.Pow(Math.Pow(orbSystem.orbList[currentIndex].position[0], 2) + Math.Pow(orbSystem.orbList[currentIndex].position[1], 2), 0.5) / 1000 + " km";
                        }
                        
                        orbSystem.calcPosition();
                        animationPictureBox.Invalidate();
                        animationPictureBox.Update();

                    }));
                    //Thread.Sleep(20);
                    i++;
                }

            });


            bw.RunWorkerAsync();
        }

        private void animationPictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (beforeStart)
            {
                return;
            }

             for (int j = 0; j < orbSystem.orbList.Count; j++)
            {
               if (orbSystem.orbList[j].isMoon)
                {
                    double x = orbSystem.orbList[j].position[0] - orbSystem.orbList[j].homePlanetForMoon.position[0];
                    double y = orbSystem.orbList[j].position[1] - orbSystem.orbList[j].homePlanetForMoon.position[1];
                    double r = Math.Pow(Math.Pow(x, 2) + Math.Pow(y, 2), 0.5);
                    double sinAlpha = y / r;
                    double cosAlpha = x / r;
                    double moonX = cosAlpha * r * 50 + orbSystem.orbList[j].homePlanetForMoon.position[0];
                    double moonY = sinAlpha * r * 50 + orbSystem.orbList[j].homePlanetForMoon.position[1];
                    e.Graphics.DrawEllipse(blackPen, (float)((moonX) * scale + offsetX - orbSystem.orbList[j].radiusForDrawing), (float)((moonY) * scale + offsetY - orbSystem.orbList[j].radiusForDrawing), (float)(2 * orbSystem.orbList[j].radiusForDrawing), (float)(2 * orbSystem.orbList[j].radiusForDrawing));
                    
                }
                else
                {
                    e.Graphics.DrawEllipse(blackPen, (float)((orbSystem.orbList[j].position[0]) * scale + offsetX - orbSystem.orbList[j].radiusForDrawing), (float)((orbSystem.orbList[j].position[1]) * scale + offsetY - orbSystem.orbList[j].radiusForDrawing), (float)(2 * orbSystem.orbList[j].radiusForDrawing), (float)(2 * orbSystem.orbList[j].radiusForDrawing));

                }
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            prepareOrbSystem();

            for (int i = 0; i < orbSystem.orbList.Count; i++)
            {
                objectsListBox.Items.Add(orbSystem.orbList[i].name);
            }
            objectsListBox.SelectedIndex = 0;

            beforeStart = false;
            limit = Int32.Parse(limitTextBox.Text);
            deltaT = Int32.Parse(deltaTTextBox.Text);
            max = orbSystem.getMaxDistance();
            minPanelDim = Math.Min(animationPictureBox.Width, animationPictureBox.Height) * 0.9;
            scale = minPanelDim / max;

            offsetX = animationPictureBox.Width / 2;
            offsetY = animationPictureBox.Height / 2;
            show();
        }

        private void objectsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentIndex = objectsListBox.SelectedIndex;
            nameTextBox.Text = orbSystem.orbList[currentIndex].name;
            massTextBox.Text = orbSystem.orbList[currentIndex].mass/1000+" kg";
            radiusTextBox.Text = orbSystem.orbList[currentIndex].radius/1000+" km";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            beforeStart = true;
            bw.CancelAsync();
            objectsListBox.Items.Clear();
        }
    }
}

