﻿using System;
using System.Collections.Generic;

namespace UkladSloneczny
{
    public class OrbSystem
    {
        public IList<Orb<Double>> orbList { get; set; }
        public Double GRAVITY_CONST { get; set; }
        public Double deltaT { get; set; }
        public Double SUN_MASS { get; set; }
        public OrbSystem(double deltaT)
        {
            orbList = new List<Orb<Double>>();
            this.deltaT = deltaT;
            GRAVITY_CONST = 6.674 * Math.Pow(10, -11);
            SUN_MASS = 1.98855 * Math.Pow(10, 30);
        }
        public Double calcVCosm(Double r, Double mass)
        {
            return Math.Pow(GRAVITY_CONST * mass / r, 0.5);
        }
        public void addPlanet(Orb<Double> planet)
        {
            orbList.Add(planet);
        }
        public Double getMaxDistance()
        {
            Double max = 0;
            int index = 0;
            for(int i=0; i<orbList.Count; i++)
            {
                for(int k=0; k<2; k++)
                {
                    if (Math.Abs(orbList[i].position[k]) > max)
                    {
                        max = orbList[i].position[k];
                        index = i;
                    }
                }
                
            }

            return 2*max+2* orbList[index].radius;
        }
        public void calcForce()
        {
            Double sumX = 0;
            Double sumY = 0;
            Double r = 0;
            for (int i=1; i< orbList.Count; i++)
            {
                Orb<Double> planet = orbList[i];
                for (int j = 0; j < orbList.Count; j++)
                {
                    if (i != j)
                    {
                        r = Math.Pow(Math.Pow(planet.position[0] - orbList[j].position[0], 2) + Math.Pow(planet.position[1] - orbList[j].position[1], 2), 0.5);
                        sumX = sumX + (orbList[j].mass / Math.Pow(r, 3)) * (orbList[j].position[0] - planet.position[0]);
                        sumY = sumY + (orbList[j].mass / Math.Pow(r, 3)) * (orbList[j].position[1] - planet.position[1]);
                    }

                    
                }


                sumX = sumX * GRAVITY_CONST * (1);
                sumY = sumY * GRAVITY_CONST * (1);
                planet.force[0] = sumX;
                planet.force[1] = sumY;
                sumX = 0;
                sumY = 0;



            }

        }
        public void calcSpeed()
        {
            calcForce();
            for (int i = 0; i < orbList.Count; i++)
            {
                Orb<Double> planet = orbList[i];
                for (int k = 0; k < 2; k++)
                {
                    planet.currentV[k] = planet.currentV[k] + planet.force[k] * deltaT;
                }
            }
        }
        public void calcPosition()
        {
            calcSpeed();
            Double[] lastPosition = new Double[2];
            for (int i = 0; i < orbList.Count; i++)
            {
                Orb<Double> planet = orbList[i];
                for (int k = 0; k < 2; k++)
                {
                    lastPosition[k] = planet.position[k];
                    planet.position[k] = planet.position[k] + planet.currentV[k]  * deltaT;
                }
                //planet.trajectory.Add(lastPosition);
            }
        }
    }
}
