﻿namespace UkladSloneczny
{
    partial class mainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.parametersGroupBox = new System.Windows.Forms.GroupBox();
            this.distanceTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.radiusTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.massTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.animationPictureBox = new System.Windows.Forms.PictureBox();
            this.objectsListBox = new System.Windows.Forms.ListBox();
            this.removeButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.limitTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.deltaTTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.speedYTextBox = new System.Windows.Forms.TextBox();
            this.speedXTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.stopButton = new System.Windows.Forms.Button();
            this.parametersGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.animationPictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // parametersGroupBox
            // 
            this.parametersGroupBox.Controls.Add(this.distanceTextBox);
            this.parametersGroupBox.Controls.Add(this.label6);
            this.parametersGroupBox.Controls.Add(this.speedYTextBox);
            this.parametersGroupBox.Controls.Add(this.label5);
            this.parametersGroupBox.Controls.Add(this.speedXTextBox);
            this.parametersGroupBox.Controls.Add(this.label4);
            this.parametersGroupBox.Controls.Add(this.radiusTextBox);
            this.parametersGroupBox.Controls.Add(this.label3);
            this.parametersGroupBox.Controls.Add(this.massTextBox);
            this.parametersGroupBox.Controls.Add(this.label2);
            this.parametersGroupBox.Controls.Add(this.nameTextBox);
            this.parametersGroupBox.Controls.Add(this.label1);
            this.parametersGroupBox.Location = new System.Drawing.Point(12, 101);
            this.parametersGroupBox.Name = "parametersGroupBox";
            this.parametersGroupBox.Size = new System.Drawing.Size(184, 218);
            this.parametersGroupBox.TabIndex = 0;
            this.parametersGroupBox.TabStop = false;
            this.parametersGroupBox.Text = "Parametry ciała niebieskiego";
            // 
            // distanceTextBox
            // 
            this.distanceTextBox.Location = new System.Drawing.Point(9, 187);
            this.distanceTextBox.Name = "distanceTextBox";
            this.distanceTextBox.Size = new System.Drawing.Size(159, 20);
            this.distanceTextBox.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 165);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Odległość od Słońca";
            // 
            // radiusTextBox
            // 
            this.radiusTextBox.Location = new System.Drawing.Point(68, 84);
            this.radiusTextBox.Name = "radiusTextBox";
            this.radiusTextBox.Size = new System.Drawing.Size(100, 20);
            this.radiusTextBox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Promień";
            // 
            // massTextBox
            // 
            this.massTextBox.Location = new System.Drawing.Point(68, 58);
            this.massTextBox.Name = "massTextBox";
            this.massTextBox.Size = new System.Drawing.Size(100, 20);
            this.massTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Masa";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(68, 32);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(100, 20);
            this.nameTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nazwa";
            // 
            // animationPictureBox
            // 
            this.animationPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.animationPictureBox.Location = new System.Drawing.Point(202, 12);
            this.animationPictureBox.Name = "animationPictureBox";
            this.animationPictureBox.Size = new System.Drawing.Size(968, 621);
            this.animationPictureBox.TabIndex = 1;
            this.animationPictureBox.TabStop = false;
            this.animationPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.animationPictureBox_Paint);
            // 
            // objectsListBox
            // 
            this.objectsListBox.FormattingEnabled = true;
            this.objectsListBox.Location = new System.Drawing.Point(12, 326);
            this.objectsListBox.Name = "objectsListBox";
            this.objectsListBox.Size = new System.Drawing.Size(184, 277);
            this.objectsListBox.TabIndex = 2;
            this.objectsListBox.SelectedIndexChanged += new System.EventHandler(this.objectsListBox_SelectedIndexChanged);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(121, 609);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 34);
            this.removeButton.TabIndex = 12;
            this.removeButton.Text = "Start";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.limitTextBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.deltaTTextBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(184, 83);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parametry układu";
            // 
            // limitTextBox
            // 
            this.limitTextBox.Location = new System.Drawing.Point(68, 45);
            this.limitTextBox.Name = "limitTextBox";
            this.limitTextBox.Size = new System.Drawing.Size(100, 20);
            this.limitTextBox.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Limit";
            // 
            // deltaTTextBox
            // 
            this.deltaTTextBox.Location = new System.Drawing.Point(68, 19);
            this.deltaTTextBox.Name = "deltaTTextBox";
            this.deltaTTextBox.Size = new System.Drawing.Size(100, 20);
            this.deltaTTextBox.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "DeltaT";
            // 
            // speedYTextBox
            // 
            this.speedYTextBox.Location = new System.Drawing.Point(68, 136);
            this.speedYTextBox.Name = "speedYTextBox";
            this.speedYTextBox.Size = new System.Drawing.Size(100, 20);
            this.speedYTextBox.TabIndex = 9;
            // 
            // speedXTextBox
            // 
            this.speedXTextBox.Location = new System.Drawing.Point(68, 110);
            this.speedXTextBox.Name = "speedXTextBox";
            this.speedXTextBox.Size = new System.Drawing.Size(100, 20);
            this.speedXTextBox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Vx";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Vy";
            // 
            // stopButton
            // 
            this.stopButton.Location = new System.Drawing.Point(12, 609);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 34);
            this.stopButton.TabIndex = 14;
            this.stopButton.Text = "STOP";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // mainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 651);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.objectsListBox);
            this.Controls.Add(this.animationPictureBox);
            this.Controls.Add(this.parametersGroupBox);
            this.Name = "mainWindow";
            this.Text = "Układ Słoneczny";
            this.parametersGroupBox.ResumeLayout(false);
            this.parametersGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.animationPictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox parametersGroupBox;
        private System.Windows.Forms.TextBox radiusTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox massTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox animationPictureBox;
        private System.Windows.Forms.ListBox objectsListBox;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.TextBox distanceTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox limitTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox deltaTTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox speedYTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox speedXTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button stopButton;
    }
}

